# ModernIPAC

Building an open-source kiosk for library catalog browsing.

## X11 Resources

- <http://alandmoore.com/blog/2011/11/05/creating-a-kiosk-with-linux-and-x11-2011-edition/>
- <https://wiki.archlinux.org/title/LXDM>
- <https://www.teddit.net/r/debian/comments/4snvdv/disable_text_ttys_in_jessie_and_beyond/>
- <https://gist.github.com/thewh1teagle/feb6f1ec221e8bad3b1207abff04c4e7>
- <https://wiki.mozilla.org/Firefox/CommandLineOptions>
- `ls -1 /sys/class/drm/*/edid`

## Wayland Resources

- <https://github.com/Hjdskes/cage>
- <https://www.mankier.com/1/swayidle#>
- <https://man.sr.ht/~kennylevinsen/greetd/#>
- <https://github.com/Hjdskes/cage/wiki/Starting-Cage-on-boot-with-systemd>
- <https://www.michelebologna.net/2019/automatic-or-unattended-upgrades-in-opensuse-centos-and-fedora-debian-and-ubuntu/>
- <https://kubic.opensuse.org/blog/2018-04-04-transactionalupdates/>
- <https://kubic.opensuse.org/blog/2018-04-20-transactionalupdates2/>
