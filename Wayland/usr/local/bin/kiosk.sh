#!/bin/sh
swayidle timeout 60 'pkill firefox' &
while true; do
  rsync -qr --delete /var/kiosk/ $HOME/
  firefox -private --kiosk https://catalog.uhls.org/
done
